# L09

L09 est une association ariégeoise qui cherche à promouvoir l'utilisation du logiciel libre.

Ce site documente toute la gestion de l'hébergement des applications web que nous proposons, afin que celles et ceux qui 
voudraient s'auto-héberger puissent s'en inspirer.

Cette documentation vise à permettre l'installation de différentes applications, mais ne documente pas leur utilisation.
Veuillez vous référer à leurs documentations officielles pour plus d'informations.