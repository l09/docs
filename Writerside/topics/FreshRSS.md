# FreshRSS

FreshRSS est un agrégateur de flux RSS et ATOM. 

## Démarrage image Docker

FreshRSS va avoir besoin de stocker quelques fichiers en local. Commencez par créer un dossier qui lui sera dédié

```
mkdir /data/freshrss
```

freshrss.docker-compose.yml
```yaml
version: '3.5'

services:

  freshrss:
    image: freshrss/freshrss:1.22.1-alpine
    environment:
      - TZ=UTC
      - CRON_MIN=1,31
    volumes:
      - ./freshrss/data:/var/www/FreshRSS/data
      - ./freshrss/extensions:/var/www/FreshRSS/extensions
    labels:
      - traefik.enable=true
      - traefik.http.routers.freshrss.rule=Host(`${RSS_URL}`)
      - traefik.http.routers.freshrss.tls.certResolver=letsencrypt
      - traefik.http.routers.freshrss.entrypoints=websecure
      - traefik.http.services.freshrss.loadbalancer.server.port=80

    restart: unless-stopped
    networks:
      - l09

networks:
  l09:
    external: true
```

Note : si vous ne souhaitez pas utiliser la connexion Keycloak, vous pouvez préférer l'image Alpine beaucoup plus
légère, en ajoutant `-alpine` au tag de l'image (ie `freshrss/freshrss:1.22.1-alpine`)

## Connexion Keycloak

Pour se connecter avec Keycloak, on va [activer OpenID Connect](https://freshrss.github.io/FreshRSS/en/admins/16_OpenID-Connect.html).

Ajoutez la variable d'environnement `OIDC_ENABLED` pour activer OIDC, et remplissez les différents paramètres

```yaml
      - OIDC_ENABLED=1
      - OIDC_PROVIDER_METADATA_URL=https://${KEYCLOAK_URL}/realms/${KEYCLOAK_REALM}/.well-known/openid-configuration
      - OIDC_CLIENT_ID=${RSS_CLIENT_ID}
      - OIDC_CLIENT_SECRET=${RSS_CLIENT_SECRET}
      - OIDC_CLIENT_CRYPTO_KEY=${RSS_CRYPTO_KEY}
      - OIDC_X_FORWARDED_HEADERS=X-Forwarded-Host X-Forwarded-Proto
      #- OIDC_SCOPES=openid email
      #- OIDC_REMOTE_USER_CLAIM=email
```

- `OIDC_PROVIDER_METADATA_URL`: lien vers la configuration Keycloak
- `OIDC_CLIENT_ID` et `OIDC_CLIENT_SECRET`: information du client créé dans Keycloak
- `OIDC_CLIENT_CRYPTO_KEY`: un texte aléatoire qui sert de clé de chiffrement
- `OIDC_SCOPES` et `OIDC_REMOTE_USER_CLAIM`: utiles si vous souhaitez utilisez un claim différent pour 
le nom d'utilisateur (par défaut : `preferred_username`)

## Configuration de FreshRSS

La configuration se fait lors du premier lancement de l'application.  
Suivez les étapes pour configurer la connexion à la base de données.  

Au choix de l'étape d'authentification, sélectionnez l'authentification HTTP pour utiliser la connexion avec Keycloak 
(Note: si vous n'avez pas configuré Keycloak précédemment, l'option ne sera pas disponible).

## Liens
- [FreshRSS](https://freshrss.org/)
- [Installation avec Docker](https://github.com/FreshRSS/FreshRSS/tree/edge/Docker)
- [Configuration OIDC](https://freshrss.github.io/FreshRSS/en/admins/16_OpenID-Connect.html)
- [Instance L09](https://freshrss.l09.fr)