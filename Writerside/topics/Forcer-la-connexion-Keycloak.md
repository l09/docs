# Forcer la connexion Keycloak

On peut utiliser Keycloak dans de nombreux services qui supportent OpenID nativement, mais certaines applications ne
le gèrent pas, ou n'ont juste pas prévu de gestion de comptes utilisateurs.

Il est possible d'ajouter un middleware à Traefik qui va s'assurer que l'utilisateur est bien connecté avec Keycloak
avant de charger l'application.

Pour pouvoir faire ça, nous allons installer le plugin [keycloakopenid](https://github.com/Gwojda/keycloakopenid).

Ouvrez le fichier traefik.config.yml, et ajoutez à la fin :

```yaml
experimental:
  plugins:
    keycloakopenid:
      moduleName: "github.com/Gwojda/keycloakopenid"
      version: "v0.1.34"
```

Dans Keycloak, créez un client pour votre application et notez son clientId et clientSecret.

Maintenant, vous pouvez ajouter un middleware dans les labels de votre application

```yaml
version: '3.9'

services:
  whoami:
    image: traefik/whoami
    labels:
      # Configuration classique de l'application
      - traefik.enable=true
      - traefik.http.routers.test.rule=Host(`${TEST_URL}`)
      - traefik.http.routers.test.tls.certResolver=letsencrypt
      - traefik.http.routers.test.entrypoints=websecure
      - traefik.http.services.test.loadbalancer.server.port=80
      # Déclaration du middleware
      # `keycloakauth` ici est le nom de l'instance de middleware qu'on crée
      # `keycloakopenid` est le nom du plugin que l'on utilise, déclaré dans la config Traefik
      - traefik.http.middlewares.keycloakauth.plugin.keycloakopenid.KeycloakURL=${KEYCLOAK_URL}
      - traefik.http.middlewares.keycloakauth.plugin.keycloakopenid.KeycloakRealm=${KEYCLOAK_REALM}
      - traefik.http.middlewares.keycloakauth.plugin.keycloakopenid.ClientID=${APP_CLIENT_ID}
      - traefik.http.middlewares.keycloakauth.plugin.keycloakopenid.ClientSecret=${APP_CLIENT_SECRET}
      # On active le middleware dans le router test
      - traefik.http.routers.test.middlewares=keycloakauth
      # Il est possible de faire passer un claim dans un header des requêtes
      - traefik.http.middlewares.keycloakauth.plugin.keycloakopenid.UserClaimName=email
      - traefik.http.middlewares.keycloakauth.plugin.keycloakopenid.UserHeaderName=X-Keycloak-User
```

## Liens
- [Keycloak Authentication Plugin](https://github.com/Gwojda/keycloakopenid)