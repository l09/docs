# Keycloak

[Keycloak](https://www.keycloak.org/) est un serveur d'authentification, nous allons l'utiliser pour avoir un unique login et mot de passe sur tous 
nos services. 

## Démarrage image Docker

keycloak.docker-compose.yml :
```yaml
version: '3.9'

services:
  keycloak:
    image: quay.io/keycloak/keycloak:23.0.1
    environment:
      - KC_DB=postgres
      - KC_DB_URL_HOST=${POSTGRES_HOST}
      - KC_DB_URL_PORT=${POSTGRES_PORT}
      - KC_DB_URL_DATABASE=${KEYCLOAK_DB_NAME}
      - KC_DB_USERNAME=${POSTGRES_USER}
      - KC_DB_PASSWORD=${POSTGRES_PASSWORD}
      - KC_HOSTNAME_STRICT=false
      - KC_HOSTNAME_URL=https://${KEYCLOAK_URL}
      - KC_HOSTNAME_ADMIN_URL=https://${KEYCLOAK_URL}
      - KC_PROXY=edge
    labels:
      - traefik.enable=true
      - traefik.http.routers.keycloak.rule=Host(`${KEYCLOAK_URL}`)
      - traefik.http.routers.keycloak.tls.certResolver=letsencrypt
      - traefik.http.routers.keycloak.entrypoints=websecure
      - traefik.http.services.keycloak.loadbalancer.server.port=8080
    restart: unless-stopped
    command: start
    networks:
      - l09
  
networks:
  l09:
    external: true
```

Plusieurs valeurs sont à remplir dans le fichier `.env`

```
# Postgres
POSTGRES_HOST=192.168.92.68
POSTGRES_PORT=5432
POSTGRES_USER=pguser
POSTGRES_PASSWORD=password1234

# Keycloak
KEYCLOAK_URL=keycloak.example.com
KEYCLOAK_DB_NAME=db-keycloak
```

On copie ces 2 fichiers dans `/srv`, et on démarre l'image

```Shell
sudo docker compose -f keycloak.docker-compose.yml up -d
```

## Initialisation du compte administrateur

Le premier démarrage va initialiser la base de données automatiquement. Une fois que c'est fait, vous pouvez
accéder au site, mais vous ne pouvez pas vous loguer.

Pour créer le premier utilisateur, modifiez le fichier `keycloak.docker-compose.yml`, et ajoutez les 2 variables
d'environnement suivantes :

```yaml
    environment:
      - KEYCLOAK_ADMIN=kcadmin
      - KEYCLOAK_ADMIN_PASSWORD=password1234
```

Relancez la commande shell pour démarrer l'image Docker, et le compte administrateur va être créé en base de données
Vous pouvez maintenant revenir sur le site et vous loguer.

Une fois que c'est bon, vous pouvez remodifier le docker-compose pour supprimer l'identifiant admin

## Initialisation d'un realm

Une fois logué dans la console d'administration, vous arrivez dans le realm `master`. Ce realm est dédié à 
l'administration du keycloak, et il est recommandé d'en utiliser un différent pour vos applications.

Cliquez sur la dropdown contenant le nom du realm "master" en haut à gauche, puis "Create realm".  
Choisissez un nom, puis cliquez sur "Create". Suivez maintenant les indications pour créer un realm.


## Liens
- [Keycloak](https://www.keycloak.org/)
- [Documentation](https://www.keycloak.org/documentation)
- [Images Docker](https://quay.io/repository/keycloak/keycloak)