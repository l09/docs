# Cobalt

Cobalt est une application permettant de télécharger des vidéos et sons de différentes plateformes.

## Démarrage image Docker

cobalt.docker-compose.yml
```yaml
version: '3.5'

services:
  cobalt-api:
    image: ghcr.io/wukko/cobalt:7
    restart: unless-stopped
    init: true
    environment:
      - apiURL=https://${COBALT_API_URL}/
      - apiName=${COBAL_API_NAME}
    labels:
      - traefik.enable=true
      - traefik.http.routers.cobalt-api.rule=HOST(`${COBALT_API_URL}`)
      - traefik.http.routers.cobalt-api.tls.certResolver=letsencrypt
      - traefik.http.routers.cobalt-api.entrypoints=websecure
      - traefik.http.services.cobalt-api.loadbalancer.server.port=9000
    networks:
      - l09

  cobalt-web:
    image: ghcr.io/wukko/cobalt:7
    restart: unless-stopped
    init: true
    environment:
        - webURL=https://${COBALT_WEB_URL}/
        - apiURL=https://${COBALT_API_URL}/
    labels:
      - traefik.enable=true
      - traefik.http.routers.cobalt-web.rule=HOST(`${COBALT_WEB_URL}`)
      - traefik.http.routers.cobalt-web.tls.certResolver=letsencrypt
      - traefik.http.routers.cobalt-web.entrypoints=websecure
      - traefik.http.services.cobalt-web.loadbalancer.server.port=9001
    networks:
      - l09

networks:
  l09:
    external: true
```

Le service Cobalt est déployé en 2 parties, le fichier docker-compose contient un service pour l'API et un pour 
l'interface web. Pour cette raison, il vous faudra définir 2 entrées dns.  
Si ici ils sont déployés en même temps, il est possible de les déployer sur 2 serveurs séparés.

## Liens
- [Projet Cobalt sur Github](https://github.com/wukko/cobalt)
- [Instance officielle](https://cobalt.tools)
- [Instance L09](https://cobalt.l09.fr)