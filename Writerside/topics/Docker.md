# Docker

Tous les services sont hébergés en utilisant Docker et Docker-Compose

[Installation de Docker sous Ubuntu](https://docs.docker.com/engine/install/ubuntu/)
[Installation du plugin Compose](https://docs.docker.com/compose/install/linux/#install-using-the-repository)

```Shell
# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

# Add the repository to Apt sources:
echo \
"deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
$(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

Les services vont devoir utiliser un même Network, on le crée dès maintenant

```Shell
sudo docker network create l09
```

Chaque service est déployé à l'aide d'un fichier [nom-service].docker-compose.yml, et éventuellement un ou plusieurs 
fichiers de configuration.

Ils sont démarrés ou mis à jour à l'aide de la commande :

```Shell
sudo docker compose -f [nom-service].docker-compose.yml up -d
```

N'oubliez pas de vérifier et de mettre à jour régulièrement la version des images utilisées. 

Certains paramètres ne seront pas inscrits directement dans le fichier docker-compose ; par exemple vous verrez 
la syntaxe `${POSTGRES_HOST}`

Ça signifie que la valeur est située dans un fichier d'environnement `.env`

```
POSTGRES_HOST=192.168.92.68
POSTGRES_PORT=5432
POSTGRES_USER=pguser
POSTGRES_PASSWORD=password1234
```




## Liens 
- [Docker](https://www.docker.com/)
- [Documentation](https://docs.docker.com/)
