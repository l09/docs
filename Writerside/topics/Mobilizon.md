# Mobilizon

[Mobilizon](https://joinmobilizon.org/fr/) est un service qui permets de trouver, créer et organiser des évènements.
Il peut servir d'alternative aux groupes et évènements Facebook ou Meetup.

*Documentation d'installation de Mobilizon en cours de rédaction*

## Image Docker

```yaml
version: '3'

services:
  mobilizon:
    image: framasoft/mobilizon:4.0.2
    environment:
      - MOBILIZON_INSTANCE_NAME=${MOBILIZON_INSTANCE_NAME}
      - MOBILIZON_INSTANCE_HOST=${MOBILIZON_URL}
      - MOBILIZON_INSTANCE_PORT=4000
      - MOBILIZON_INSTANCE_EMAIL=${MOBILIZON_INSTANCE_EMAIL}
      - MOBILIZON_REPLY_EMAIL=${MOBILIZON_INSTANCE_EMAIL}
      - MOBILIZON_ADMIN_EMAIL=${MOBILIZON_INSTANCE_EMAIL}
      - MOBILIZON_INSTANCE_REGISTRATIONS_OPEN=true
      - MOBILIZON_DATABASE_USERNAME=${POSTGRES_USER}
      - MOBILIZON_DATABASE_PASSWORD=${POSTGRES_PASSWORD}
      - MOBILIZON_DATABASE_DBNAME=${MOBILIZON_POSTGRES_DB}
      - MOBILIZON_DATABASE_HOST=${POSTGRES_HOST}
      - MOBILIZON_DATABASE_PORT=${POSTGRES_PORT}
      - MOBILIZON_INSTANCE_SECRET_KEY_BASE
      - MOBILIZON_INSTANCE_SECRET_KEY
      - MOBILIZON_SMTP_SERVER=${SMTP_SERVER}
      - MOBILIZON_SMTP_HOSTNAME=${SMTP_SERVER}
      - MOBILIZON_SMTP_PORT=${SMTP_PORT}
      - MOBILIZON_SMTP_SSL=${SMTP_SSL}
      - MOBILIZON_SMTP_USERNAME=${SMTP_USERNAME}
      - MOBILIZON_SMTP_PASSWORD=${SMTP_PASSWORD}
      - MOBILIZON_KEYCLOAK_URL
      - MOBILIZON_KEYCLOAK_CLIENT_ID
      - MOBILIZON_KEYCLOAK_CLIENT_SECRET
    labels:
      - traefik.enable=true
      - traefik.http.routers.mobilizon.rule=Host(`${MOBILIZON_URL}`)
      - traefik.http.routers.mobilizon.tls.certResolver=letsencrypt
      - traefik.http.routers.mobilizon.entrypoints=websecure
      - traefik.http.services.mobilizon.loadbalancer.server.port=4000
    volumes:
      - /data/mobilizon/uploads:/var/lib/mobilizon/uploads
      - ./mobilizon/config.exs:/etc/mobilizon/config.exs:ro
      # - /data/mobilizon/GeoLite2-City.mmdb:/var/lib/mobilizon/geo_db/GeoLite2-City.mmdb
    networks:
      - l09

networks:
  l09:
    external: true
```

## Configuration Keycloak

Configuration dans config.exs. Ajouter à la fin :

```
config :ueberauth,
  Ueberauth,
  providers: [
    keycloak: {Ueberauth.Strategy.Keycloak, [default_scope: "openid email"]}
  ]

config :mobilizon, :auth,
  oauth_consumer_strategies: [
    {:keycloak, "Connexion Keycloak"}
  ]
  
keycloak_url = System.get_env("MOBILIZON_KEYCLOAK_URL")

config :ueberauth, Ueberauth.Strategy.Keycloak.OAuth,
  client_id: System.get_env("MOBILIZON_KEYCLOAK_CLIENT_ID"),
  client_secret: System.get_env("MOBILIZON_KEYCLOAK_CLIENT_SECRET"),
  site: keycloak_url,
  authorize_url: "#{keycloak_url}/protocol/openid-connect/auth",
  token_url: "#{keycloak_url}/protocol/openid-connect/token",
  userinfo_url: "#{keycloak_url}/protocol/openid-connect/userinfo",
  token_method: :post  
```

- `MOBILIZON_KEYCLOAK_URL`: chemin vers le realm keycloak "https://keycloak.example.fr/realms/my_realm"
- `MOBILIZON_KEYCLOAK_CLIENT_ID` et `MOBILIZON_KEYCLOAK_CLIENT_SECRET`: information du client créé dans Keycloak

## Liens
- [Mobilizon](https://joinmobilizon.org/fr/)
- [Installation avec Docker](https://docs.joinmobilizon.org/administration/install/docker/)