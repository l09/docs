# Sauvegardes

Les fichiers utilisés sur le serveur sont stockés dans le dossier `/srv`. Ce dossier est sauvegardé automatiquement à 
intervalle régulier - en cas de problème serveur, il suffit de récupérer la sauvegarde sur un nouveau serveur et de
relancer la commande Docker compose pour relancer les services.

## Installation et configuration de RClone

Nous utilisons [RClone](https://rclone.org) pour envoyer les fichiers de sauvegarde vers un stockage externe.

L'installation se fait en téléchargeant et exécutant un script :

```Shell
sudo -v ; curl https://rclone.org/install.sh | sudo bash
```
Ensuite, nous allons configurer le répertoire de stockage distant (dans notre cas, un Object Storage Scaleway compatible
S3, mais vous pouvez utiliser [tout service compatible](https://rclone.org/docs/))

Lancez la commande `rclone config` et suivez les étapes demandées pour configurer le stockage distant.


## Création d'une sauvegarde

La création d'une sauvegarde se fait en 2 parties :

```Shell
tar cvzf /backups/bak-$(date '+%\Y-%m-%d').tar.gz /srv
rclone move /backups remote:backup-bucket
```

La première commande crée une archive du dossier `/srv`, qui sera stockée dans le dossier `/backups` (qui doit avoir 
déjà été créé), avec un nom contenant la date du jour (afin d'avoir un fichier différent par jour).

La seconde commande utilise rclone pour envoyer l'archive (ie le contenu du dossier `/backups`) dans le bucket S3.
Le dernier paramètre `remote:backup-bucket` est en 2 parties, `remote` est le nom du répertoire distant donné lors de la
configuration de rclone, `backup-bucket` est le nom du bucket sur le service S3.

## Automatisation des sauvegardes quotidiennes

Maintenant qu'on est capable de créer une sauvegarde, on va utiliser `cron` pour en créer une automatiquement tous les 
jours.

On va modifier le fichier crontab pour créer un job :

```Shell
crontab -e
```

Dans l'éditeur qui s'ouvre, ajoutez la ligne suivante et enregistrez :

```
0 4 * * * tar cvzf /backups/bak-$(date '+\%Y-\%m-\%d').tar.gz /srv && rclone move /backups remote:backup-bucket
```

Ceci permets d'exécuter chaque nuit à 4h du matin un backup, et de l'envoyer vers le bucket S3.

## Liens
- [Documentation RClone](https://rclone.org/docs)
- [Crontab.guru](https://crontab.guru/#0_4_*_*_*)
- [Why is my crontab not working, and how can I troubleshoot it?](https://serverfault.com/questions/449651/why-is-my-crontab-not-working-and-how-can-i-troubleshoot-it)

## Note

Ce type de sauvegarde fonctionne, mais n'est pas efficace.

TODO : utiliser des logiciels plus spécialisés, type [Restic](https://restic.net/) ou [BorgBackup](https://www.borgbackup.org/)
