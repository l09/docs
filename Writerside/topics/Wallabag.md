# Wallabag

Wallabag permets d'enregistrer et de classer des articles venant d'internet pour les lires plus tard.

Il est possible de générer des flux RSS pour trouver ces articles dans FreshRSS.

Variables d'environnement à définir
```
POSTGRES_HOST=<hôte postgresql>
POSTGRES_PORT=<port postgresql>
POSTGRES_USER=<user postgresql>
POSTGRES_PASSWORD=<mot de passe postgresql>
SYMFONY__ENV__SECRET=<une chaine de caractères aléatoire>
WALLABAG_URL=<l'url publique>
```

wallabag.docker-compose.yml
```yaml
version: '3.5'

services:

  wallabag:
    image: wallabag/wallabag:2.6.7
    environment:
      - SYMFONY__ENV__DATABASE_DRIVER=pdo_pgsql
      - SYMFONY__ENV__DATABASE_HOST=${POSTGRES_HOST}
      - SYMFONY__ENV__DATABASE_PORT=${POSTGRES_PORT}
      - SYMFONY__ENV__DATABASE_NAME=wallabag
      - SYMFONY__ENV__DATABASE_USER=${POSTGRES_USER}
      - SYMFONY__ENV__DATABASE_PASSWORD=${POSTGRES_PASSWORD}
      - SYMFONY__ENV__SECRET
      - SYMFONY__ENV__LOCALE=fr
      - SYMFONY__ENV__FOSUSER_REGISTRATION=true
      - SYMFONY__ENV__FOSUSER_CONFIRMATION=false
      - SYMFONY__ENV__DOMAIN_NAME=https://${WALLABAG_URL}
      - POSTGRES_PASSWORD
      - POSTGRES_USER
      - SYMFONY__ENV__SERVER_NAME=L09
    labels:
      - traefik.enable=true
      - traefik.http.routers.wallabag.rule=Host(`${WALLABAG_URL}`)
      - traefik.http.routers.wallabag.tls.certResolver=letsencrypt
      - traefik.http.routers.wallabag.entrypoints=websecure
      - traefik.http.services.wallabag.loadbalancer.server.port=80

    restart: unless-stopped
    networks:
      - l09

networks:
  l09:
    external: true
```


## Installation

Une fois l'image démarrée, faites `docker ps` pour récupérer l'id de l'image, puis lancez la commande suivante pour
initialiser le service :

    docker exec -t <id_wallabag_container> /var/www/wallabag/bin/console wallabag:install --env=prod

Vous pouvez par la suite donner des droits à un utilisateur en lui donnant le role `ROLE_ADMIN` ou `ROLE_SUPER_ADMIN`

    docker exec -t <id_wallabag_container> /var/www/wallabag/bin/console fos:user:promote <user> <role> --env=prod

## Liens

- [Site officiel](https://wallabag.org/)
- [Documentation](https://doc.wallabag.org/fr/)
- [Image Docker](https://hub.docker.com/r/wallabag/wallabag/)
- [Instance L09](https://wallabag.l09.fr/)
