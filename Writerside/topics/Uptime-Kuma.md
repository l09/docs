# Uptime Kuma

Uptime Kuma est un outil de monitoring qui va surveiller toutes vos applications et vous prévenir si l'une d'elle
cesse de fonctionner.

Contrairement aux autres services qu'on peut installer sur un serveur unique, ici il est conseillé d'avoir un serveur
séparé pour installer cet outil. En effet, si votre serveur plante totalement, Uptime Kuma ne pourra pas
vous prévenir s'il est installé dessus.

## Démarrage image Docker

uptime-kuma.docker-compose.yml
```yaml
version: '3.5'

services:
  uptime-kuma:
    image: louislam/uptime-kuma:latest
    volumes:
      - /data/uptime-kuma:/app/data
    labels:
      - traefik.enable=true
      - traefik.http.routers.kuma.rule=HOST(`${KUMA_URL}`)
      - traefik.http.routers.kuma.tls.certResolver=letsencrypt
      - traefik.http.routers.kuma.entrypoints=websecure
      - traefik.http.services.kuma.loadbalancer.server.port=3001
    restart: unless-stopped
    networks:
      - l09

networks:
  l09:
    external: true
```

Si vous souhaitez créer une page de statut publique, vous avez la possibilité de la présenter sur une url dédiée.

Pour cela, vous allez devoir préciser à Traefik que l'appli répond à 2 url :

```yaml
      - traefik.http.routers.kuma.rule=HOST(`${KUMA_ADMIN_URL}`) || HOST(`${KUMA_STATUS_URL}`)
```

## Liens
- [Site officiel](https://uptime.kuma.pet/)
- [Page de statut des services L09](https://status.l09.fr/)