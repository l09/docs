# Traefik

[Traefik](https://traefik.io/traefik/) est un reverse-proxy : il sert de point d'entrée sur le serveur à toutes les
requêtes web, et renvoie vers les différents services. Il est aussi responsable d'assurer la connexion sécurisée en
https.

Traefik démarre à l'aide du fichier docker-compose suivant :

traefik.docker-compose.yml :

```yaml
version: '3.5'

services:
  traefik:
    # The official v3 Traefik Docker image
    image: traefik:v3.0
    ports:
      - "80:80"
      - "443:443"
    volumes:
      # So that Traefik can listen to the Docker events
      - /var/run/docker.sock:/var/run/docker.sock
      - ./traefik.config.yml:/etc/traefik/traefik.yml
      - ./acme.json:/certs/acme.json
    restart: unless-stopped
    networks:
      - l09

networks:
  l09:
    external: true
```

Il est associé à un fichier de configuration déployé en même temps

traefik.config.yml :

```yaml
api:
  dashboard: false
  insecure: false

entryPoints:
  web:
    address: :80
    http:
      redirections:
        entryPoint:
          to: websecure
          scheme: https
  websecure:
    address: :443
    http:
      tls:
        certResolver="letsencrypt"

providers:
  docker:
    watch: true
    exposedByDefault: false
    network: web

certificatesResolvers:
  letsencrypt:
    acme:
      email: admin@example.com
      storage: /certs/acme.json
      httpChallenge:
        # used during the challenge
        entryPoint: web
```

À noter sur cette configuration :
- `api`: Traefik propose un dashboard pour surveiller les services actifs. Ce dashboard est désactivé ici
- `entryPoints`: ici nous définissons les points d'entrée, http sur le port 80, et https sur le port 443. Les requêtes
en http sont redirigées vers https ; https utilise Let's Encrypt pour obtenir les certificats tls (configuré plus bas)
- `providers`: ici on dit à Traefik de surveiller les images Docker qui tournent sur le serveur

La configuration de Traefik n'a ici aucune connaissance des services vers lesquels il doit router les requêtes.  
C'est chaque service qui enverra sa propre configuration à Traefik au démarrage, à l'aide de labels associés au 
container.

On aura par exemple ces labels associés à une image :

```yaml
    labels:
      - traefik.enable=true
      - traefik.http.routers.keycloak.rule=Host(`www.example.com`)
      - traefik.http.routers.keycloak.tls.certResolver=letsencrypt
      - traefik.http.routers.keycloak.entrypoints=websecure
      - traefik.http.services.keycloak.loadbalancer.server.port=8000
```
 
Ces labels seront inclus dans les fichiers docker-compose de chaque service décrit dans cette documentation.

Traefik va utiliser un 3ème fichier `acme.json` pour stocker les certificats https. On l'initialise avec un objet
json vide

```Shell
echo "{}"  > acme.json
chmod 600 acme.json
```

On copie ces 3 fichiers dans `/srv`, et on démarre traefik

```Shell
sudo docker compose -f traefik.docker-compose.yml up -d
```

## Liens
- [Traefik](https://traefik.io/traefik/)
- [Documentation](https://doc.traefik.io/traefik/)
- [Traefik sur Docker hub](https://hub.docker.com/_/traefik)
