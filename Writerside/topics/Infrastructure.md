# Infrastructure

## Serveur hôte

Le serveur est une machine virtuelle sous Ubuntu.

## Pare-feu

Le pare-feu est activé, et laisse uniquement entrer les connexions http, https, ainsi que ssh pour l'administration.

```Shell
sudo ufw allow ssh
sudo ufw allow http
sudo ufw allow https

sudo ufw default deny incoming
sudo ufw default allow outgoing

sudo ufw enable

sudo ufw status 
```

*TODO: il est probablement préférable de bloquer le port ssh sur l'ip fixe des personnes ayant besoin d'y accéder* 


## Base de données

Certains services utilisent une base de données Postgresql.

Les bases sont stockées sur une instance Postgresql 15 Scaleway managée

*TODO : configurer des bases de données Postgresql/mysql localement*

## Docker

Les services sont déployés sur ce serveur à l'aide de Docker

Voir [page dédiée à Docker](Docker.md)


## Services web

Les applications web se basent sur les services suivants, installés sur le serveur
- [Traefik](Traefik.md) reverse-proxy
- [Keycloak](Keycloak.md) SSO
