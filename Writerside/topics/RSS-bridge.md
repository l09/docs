# RSS-bridge

RSS-bridge est une application web qui génère des flux RSS pour des sites web qui n'en ont pas.

Nous allons ici voir comment l'installer et configurer FreshRSS pour chercher automatiquement dans RSS-bridge pour
les sites web supportés.

## Démarrage image Docker

Ouvrez le fichier `freshrss.docker-compose.yml`, et ajoutez le service suivant :

```yaml
  rss-bridge:
    image: rssbridge/rss-bridge:latest
    volumes:
      - ./freshrss/rssbridge:/config
    restart: unless-stopped
    networks:
      - l09
```

Étant donné que nous souhaitons utiliser le bridge à travers FreshRSS, nous n'avons pas configuré Traefik pour rendre
l'application disponible directement par les utilisateurs.

## Configuration

Nous avons besoin de modifier la configuration, pour pouvoir activer tous les bridges que nous souhaitons.

Récupérer le fichier [config.default.ini.php](https://github.com/RSS-Bridge/rss-bridge/blob/master/config.default.ini.php),
et copiez le dans `/srv/freshrss/rssbridge/config.ini.php`.

Vous pouvez chercher les lignes `enabled_bridges[] =` pour activer ou désactiver les bridges que vous souhaitez, ou
vous pouvez tous les activer en ajoutant la ligne `enabled_bridges[] = *`

## Intégration à FreshRss

Pour intégrer le bridge à FreshRSS, nous avons besoin d'installer 
[cette extension](https://github.com/DevonHess/FreshRSS-Extensions/tree/main/xExtension-RssBridge).

Récupérer le code de l'extension sur Github, et copiez les fichiers le dossier dans le dossier 
`/data/freshrss/extensions/RssBridge`.

Retournez dans FreshRSS, et allez dans la page de configuration, et cherchez la page Extensions.

L'extension RSS-Bridge doit apparaitre. Assurez vous qu'elle soit active, et cliquez sur sa configuration pour
ajouter l'url du bridge : `http://rss-bridge`

## Liens
- [RSS Bridge](https://rss-bridge.org/)
- [Extension RSS-Bridge pour FreshRSS](https://github.com/DevonHess/FreshRSS-Extensions/tree/main/xExtension-RssBridge)