# PDF Signature

[PDF Signature](https://github.com/24eme/signaturepdf) est une application web qui permets de signer, d'organiser les 
pages, de modifier les métadonnées et de compresser des fichiers PDF.

## Démarrage image Docker

pdf.docker-compose.yml: 

```yaml
version: '3.8'

services:
  pdf:
    image: xgaia/signaturepdf:1.5.0
    environment:
      - SERVERNAME=${PDF_URL}
    labels:
      - traefik.enable=true
      - traefik.http.routers.pdf.rule=HOST(`${PDF_URL}`)
      - traefik.http.routers.pdf.tls.certResolver=letsencrypt
      - traefik.http.routers.pdf.entrypoints=websecure
      - traefik.http.services.pdf.loadbalancer.server.port=80
    restart: unless-stopped
    networks:
      - l09

networks:
  l09:
    external: true
```

Il faut inscrire dans le fichier `.env` la variable d'environnement `PDF_URL`, dont la valeur est l'url

L'application est anonyme, et ne conserve aucune données - nous n'avons donc presque rien à configurer, hormis  l'url
de l'application.

Certaines options peuvent être configuées, vous pouvez obtenir plus d'informations sur 
[la documentation d'installation](https://github.com/24eme/signaturepdf/blob/master/installation.md#configuration)

## Liens
- [Repo Github](https://github.com/24eme/signaturepdf/)
- [Instance principale](https://pdf.24eme.fr/signature)
- [Instance L09](https://pdf.l09.fr)

